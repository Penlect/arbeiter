import pytest

from arbeiter import points


def test_points_millennium():
    # Raw score equal to standard should give one thousand points.
    standard = 1234
    assert points(standard, standard) == 1_000


def test_points_spoken():
    standard = 47.3
    assert points(100, standard, type_='spoken') == 473


def test_points_speed_cards():
    standard = 8030

    # 52 correct cards and max time should give same result
    assert points(-52, standard, type_='speed_cards') == 111.39728448603724
    assert points(300, standard, type_='speed_cards') == 111.39728448603724

    assert points(0, standard, type_='speed_cards') == 0

    with pytest.raises(ValueError):
        points(-53, standard, type_='speed_cards')