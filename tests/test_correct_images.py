
import pytest
from arbeiter import correct_images

m_data = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]


def test_all_correct():
    r_data = m_data
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct'}
    assert count['correct'] == 10
    assert raw_score == 10


def test_1_gap():
    r_data = [1, 2, -1, 4, 5, 1, 2, 3, 4, 5]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap'}
    assert count['correct'] == 9
    assert count['gap'] == 1
    assert raw_score == 4


def test_2_gap():
    r_data = [1, -1, -1, 4, 5, 1, 2, 3, 4, 5]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap'}
    assert count['correct'] == 8
    assert count['gap'] == 2
    assert raw_score == 4


def test_1_wrong():
    r_data = [1, 1, 3, 4, 5, 1, 2, 3, 4, 5]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 9
    assert count['wrong'] == 1
    assert raw_score == 4


def test_2_wrong():
    r_data = [1, 1, 1, 4, 5, 1, 2, 3, 4, 5]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 8
    assert count['wrong'] == 2
    assert raw_score == 4


def test_all_not_reached():
    r_data = [-1]*10
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'not_reached'}
    assert count['not_reached'] == 10
    assert raw_score == 0


def test_last_row_not_reached():
    r_data = [1, 2, 3, 4, 5] + [-1]*5
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'not_reached'}
    assert count['correct'] == 5
    assert count['not_reached'] == 5
    assert raw_score == 5


def test_3_not_reached():
    r_data = [1, 2, 3, 4, 5, 1, 2, -1, -1, -1]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'not_reached'}
    assert count['correct'] == 7
    assert count['not_reached'] == 3
    assert raw_score == 4

def test_1_gap_1_wrong_1_not_reached():
    r_data = [1, 1, 3, 4, 5, 1, 2, -1, 4, -1]
    raw_score, count, c_data = correct_images(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}
    assert count['gap'] == 1
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert count['correct'] == 7
    assert raw_score == 0


def test_too_long_recall():
    with pytest.raises(ValueError):
        correct_images(m_data, m_data*2)


def test_empty():
    with pytest.raises(ValueError):
        correct_images([], [])
