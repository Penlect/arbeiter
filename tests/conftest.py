
import random
import pytest
import numpy as np

@pytest.fixture
def row(request):
    return list(range(40))


@pytest.fixture
def empty_value(request):
    if request.module.__name__ == 'test_correct_words':
        return ''
    else:
        return -1


@pytest.fixture
def row_1_gap(row, empty_value):
    row = row.copy()
    for index in random.sample(list(range(len(row) - 1)), 1):
        row[index] = empty_value
    return row


@pytest.fixture
def row_2_gap(row, empty_value):
    row = row.copy()
    for index in random.sample(list(range(len(row) - 1)), 2):
        row[index] = empty_value
    return row


@pytest.fixture
def row_1_wrong(row):
    row = row.copy()
    for index in random.sample(list(range(len(row))), 1):
        row[index] += 1
    return row


@pytest.fixture
def row_2_wrong(row):
    row = row.copy()
    for index in random.sample(list(range(len(row))), 2):
        row[index] += 1
    return row


@pytest.fixture
def row_not_reached(row, empty_value):
    return [empty_value for _ in row]


@pytest.fixture
def row_7_not_reached(row, empty_value):
    row = row.copy()
    row[-7:] = [empty_value]*7
    return row


@pytest.fixture
def row_1_not_reached(row, empty_value):
    row = row.copy()
    row[-1] = empty_value
    return row


@pytest.fixture
def row_1_gap_1_not_reached(row, empty_value):
    row = row.copy()
    row[0] = empty_value
    row[-1] = empty_value
    return row


@pytest.fixture
def row_2_gap_1_not_reached(row, empty_value):
    row = row.copy()
    row[0] = empty_value
    row[1] = empty_value
    row[-1] = empty_value
    return row


@pytest.fixture
def row_1_wrong_1_not_reached(row, empty_value):
    row = row.copy()
    row[0] += 1
    row[-1] = empty_value
    return row


@pytest.fixture
def row_2_wrong_1_not_reached(row, empty_value):
    row = row.copy()
    row[0] += 1
    row[1] += 1
    row[-1] = empty_value
    return row

