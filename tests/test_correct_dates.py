
import pytest
from arbeiter import correct_dates

m_data = [1988, 2002, 2003, 2004, 2005]

def _make_r_data(m):
    return m.copy()


def test_all_correct():
    r_data = _make_r_data(m_data)
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 5
    assert count['correct'] == 5
    assert {e.name for e in c_data} == {'correct'}


def test_1_gap():
    r_data = _make_r_data(m_data)
    r_data[1] = -1
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 4
    assert count['gap'] == 1
    assert count['correct'] == 4
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_2_gap():
    r_data = _make_r_data(m_data)
    r_data[1] = -1
    r_data[3] = -1
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 3
    assert count['gap'] == 2
    assert count['correct'] == 3
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_1_wrong():
    r_data = _make_r_data(m_data)
    r_data[1] = 123
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 4
    assert count['wrong'] == 1
    assert count['correct'] == 4
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_2_wrong():
    r_data = _make_r_data(m_data)
    r_data[0] = 123
    r_data[2] = 123
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 2
    assert count['wrong'] == 2
    assert count['correct'] == 3
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_all_not_reached():
    raw_score, count, c_data = correct_dates(m_data, [-1]*5)
    assert raw_score == 0
    assert count['not_reached'] == 5
    assert {e.name for e in c_data} == {'not_reached'}


def test_3_not_reached():
    r_data = _make_r_data(m_data)
    r_data[-3:] = [-1]*3
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 2
    assert count['not_reached'] == 3
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_gap_1_wrong_1_not_reached():
    r_data = _make_r_data(m_data)
    r_data[0] = -1
    r_data[1] = 123
    r_data[-1] = -1
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 2
    assert count['gap'] == 1
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert count['correct'] == 2
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}


def test_1_gap_2_wrong():
    r_data = _make_r_data(m_data)
    r_data[0] = -1
    r_data[1] = 123
    r_data[2] = 123
    raw_score, count, c_data = correct_dates(m_data, r_data)
    assert raw_score == 1
    assert count['gap'] == 1
    assert count['wrong'] == 2
    assert count['correct'] == 2
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong'}


def test_all_wrong():
    raw_score, count, c_data = correct_dates(m_data, [123]*5)
    assert raw_score == 0
    assert count['wrong'] == 5
    assert {e.name for e in c_data} == {'wrong'}


def test_too_long_recall():
    r_data = _make_r_data(m_data)
    with pytest.raises(ValueError):
        correct_dates(m_data, r_data*2)


def test_empty():
    with pytest.raises(ValueError):
        correct_dates([], [])
