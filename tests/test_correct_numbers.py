
import numpy as np
import pytest
from arbeiter import correct_numbers

N = 40


@pytest.mark.parametrize("row_length", [15, 30, 40, 50])
def test_perfect_score(row_length):
    for i in range(row_length, 500, row_length):
        memo_data = np.arange(i)
        raw_score, count, c_data = correct_numbers(memo_data, memo_data,
                                                   row_length)
        assert raw_score == i
        assert count['correct'] == i
        assert {e.name for e in c_data} == {'correct'}


@pytest.mark.parametrize("row_length", [15, 30, 40])
def test_not_reached(row_length):
    for i in range(row_length, 100, row_length):
        memo_data = np.arange(i)
        recall_data = memo_data.copy()
        for nr_not_reached in range(1, min(row_length*2, i)):
            recall_data[-nr_not_reached:] = -1
            raw_score, count, c_data = correct_numbers(
                memo_data, recall_data, row_length)
            assert raw_score == i - nr_not_reached
            assert count['correct'] == raw_score
            assert count['not_reached'] == nr_not_reached
            assert {e.name for e in c_data} <= {'correct', 'not_reached'}


def test_all_correct(row):
    raw_score, count, c_data = correct_numbers(row, row, N)
    assert raw_score == N
    assert count['correct'] == N
    assert {e.name for e in c_data} == {'correct'}


def test_1_gap(row, row_1_gap):
    raw_score, count, c_data = correct_numbers(row, row_1_gap, N)
    assert raw_score == N/2
    assert count['gap'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_2_gap(row, row_2_gap):
    raw_score, count, c_data = correct_numbers(row, row_2_gap, N)
    assert raw_score == 0
    assert count['gap'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_1_wrong(row, row_1_wrong):
    raw_score, count, c_data = correct_numbers(row, row_1_wrong, N)
    assert raw_score == N/2
    assert count['wrong'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_2_wrong(row, row_2_wrong):
    raw_score, count, c_data = correct_numbers(row, row_2_wrong, N)
    assert raw_score == 0
    assert count['wrong'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_all_not_reached(row, row_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_not_reached, N)
    assert raw_score == 0
    assert count['not_reached'] == N
    assert {e.name for e in c_data} == {'not_reached'}


def test_7_not_reached(row, row_7_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_7_not_reached, N)
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_not_reached(row, row_1_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_1_not_reached, N)
    assert raw_score == N - 1
    assert count['correct'] == raw_score
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_gap_1_not_reached(row, row_1_gap_1_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_1_gap_1_not_reached, N)
    assert raw_score == np.ceil((N - 1)/2)
    assert count['correct'] == N - 2
    assert count['gap'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_1_wrong_1_not_reached(row, row_1_wrong_1_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_1_wrong_1_not_reached,
                                               N)
    assert raw_score == np.ceil((N - 1)/2)
    assert count['correct'] == N - 2
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_2_gap_1_not_reached(row, row_2_gap_1_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_2_gap_1_not_reached,
                                               N)
    assert raw_score == 0
    assert count['correct'] == N - 3
    assert count['gap'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_2_wrong_1_not_reached(row, row_2_wrong_1_not_reached):
    raw_score, count, c_data = correct_numbers(row, row_2_wrong_1_not_reached,
                                               N)
    assert raw_score == 0
    assert count['correct'] == N - 3
    assert count['wrong'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_mix(row, row_1_gap, row_2_gap, row_1_wrong, row_2_wrong,
             row_not_reached, row_7_not_reached):
    memo_data = row*7
    recall_data = (row + row_1_gap + row_2_gap + row_1_wrong + row_2_wrong +
                   row_7_not_reached + row_not_reached)
    raw_score, count, c_data = correct_numbers(memo_data, recall_data, N)
    assert raw_score == N + N/2 + N/2 + N - 7
    assert count['gap'] == 3
    assert count['wrong'] == 3
    assert count['not_reached'] == 7 + N
    assert count['correct'] == N*6 - 1 - 2 - 1 - 2 - 7
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}


def test_shorter(row):
    raw_score, count, c_data = correct_numbers(row, row[:-7], N)
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_too_long_recall(row):
    with pytest.raises(ValueError):
        correct_numbers(row, row*2, N)


def test_negative_row_length(row):
    with pytest.raises(ValueError):
        correct_numbers(row, row, 0)
    with pytest.raises(ValueError):
        correct_numbers(row, row, -1)


def test_empty():
    with pytest.raises(ValueError):
        correct_numbers([], [], N)


def test_exactly_1_wrong():
    raw_score, count, c_data = correct_numbers([0]*40, [1] + [-1]*39, 40)
    assert raw_score == 0
    assert count['wrong'] == 1
    assert count['not_reached'] == 39
    assert {e.name for e in c_data} == {'wrong', 'not_reached'}


def test_exactly_1_wrong_1_correct():
    raw_score, count, c_data = correct_numbers([0]*40, [1, 0] + [-1]*38, 40)
    assert raw_score == 1
    assert count['correct'] == 1
    assert count['wrong'] == 1
    assert count['not_reached'] == 38
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}
