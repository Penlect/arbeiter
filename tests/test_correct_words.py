
import numpy as np
import pytest
from arbeiter import correct_words, naive_spellcheck

N = 40


def spelling_mistake_func(m_word, r_word):
    return m_word == r_word[::-1] and len(m_word) > 1


@pytest.mark.parametrize("row_length", [15, 30, 40, 50])
def test_perfect_score(row_length):
    for i in range(row_length, 500, row_length):
        memo_data = np.arange(i)
        raw_score, count, c_data = correct_words(memo_data, memo_data,
                                                 row_length)
        assert raw_score == i
        assert count['correct'] == i
        assert {e.name for e in c_data} == {'correct'}


@pytest.mark.parametrize("row_length", [15, 30, 40])
def test_not_reached(row_length):
    for i in range(row_length, 100, row_length):
        memo_data = np.arange(i)
        recall_data = list(memo_data.copy())
        for nr_not_reached in range(1, min(row_length*2, i)):
            recall_data[-nr_not_reached:] = ['']*nr_not_reached
            raw_score, count, c_data = correct_words(memo_data, recall_data,
                                                     row_length)
            assert raw_score == i - nr_not_reached
            assert count['correct'] == raw_score
            assert count['not_reached'] == nr_not_reached
            assert {e.name for e in c_data} <= {'correct', 'not_reached'}


def test_all_correct(row):
    raw_score, count, c_data = correct_words(row, row, N)
    assert raw_score == N
    assert count['correct'] == N
    assert {e.name for e in c_data} == {'correct'}


def test_1_gap(row, row_1_gap):
    raw_score, count, c_data = correct_words(row, row_1_gap, N)
    assert raw_score == N/2
    assert count['gap'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_2_gap(row, row_2_gap):
    raw_score, count, c_data = correct_words(row, row_2_gap, N)
    assert raw_score == 0
    assert count['gap'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_1_wrong(row, row_1_wrong):
    raw_score, count, c_data = correct_words(row, row_1_wrong, N)
    assert raw_score == N/2
    assert count['wrong'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_2_wrong(row, row_2_wrong):
    raw_score, count, c_data = correct_words(row, row_2_wrong, N)
    assert raw_score == 0
    assert count['wrong'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_all_not_reached(row, row_not_reached):
    raw_score, count, c_data = correct_words(row, row_not_reached, N)
    assert raw_score == 0
    assert count['not_reached'] == N
    assert {e.name for e in c_data} == {'not_reached'}


def test_7_not_reached(row, row_7_not_reached):
    raw_score, count, c_data = correct_words(row, row_7_not_reached, N)
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_not_reached(row, row_1_not_reached):
    raw_score, count, c_data = correct_words(row, row_1_not_reached, N)
    assert raw_score == N - 1
    assert count['correct'] == raw_score
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_gap_1_not_reached(row, row_1_gap_1_not_reached):
    raw_score, count, c_data = correct_words(row, row_1_gap_1_not_reached, N)
    assert raw_score == np.ceil((N - 1)/2)
    assert count['correct'] == N - 2
    assert count['gap'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_1_wrong_1_not_reached(row, row_1_wrong_1_not_reached):
    raw_score, count, c_data = correct_words(row, row_1_wrong_1_not_reached,
                                             N)
    assert raw_score == np.ceil((N - 1)/2)
    assert count['correct'] == N - 2
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_2_gap_1_not_reached(row, row_2_gap_1_not_reached):
    raw_score, count, c_data = correct_words(row, row_2_gap_1_not_reached,
                                             N)
    assert raw_score == 0
    assert count['correct'] == N - 3
    assert count['gap'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_2_wrong_1_not_reached(row, row_2_wrong_1_not_reached):
    raw_score, count, c_data = correct_words(row, row_2_wrong_1_not_reached,
                                             N)
    assert raw_score == 0
    assert count['correct'] == N - 3
    assert count['wrong'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_mix(row, row_1_gap, row_2_gap, row_1_wrong, row_2_wrong,
             row_not_reached, row_7_not_reached):
    memo_data = row*7
    recall_data = (row + row_1_gap + row_2_gap + row_1_wrong + row_2_wrong +
                   row_7_not_reached + row_not_reached)
    raw_score, count, c_data = correct_words(memo_data, recall_data, N)
    assert raw_score == N + N/2 + N/2 + N - 7
    assert count['gap'] == 3
    assert count['wrong'] == 3
    assert count['not_reached'] == 7 + N
    assert count['correct'] == N*6 - 1 - 2 - 1 - 2 - 7
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}


def test_shorter(row):
    raw_score, count, c_data = correct_words(row, row[:-7], N)
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_too_long_recall(row):
    with pytest.raises(ValueError):
        correct_words(row, row*2, N)


def test_negative_row_length(row):
    with pytest.raises(ValueError):
        correct_words(row, row, 0)
    with pytest.raises(ValueError):
        correct_words(row, row, -1)


def test_empty():
    with pytest.raises(ValueError):
        correct_words([], [], N)


def test_spelling_mistake_fixture():
    m_data = ['asdf', '123', 'qwer']
    r_data = ['f3f', '321', 'f4f4']
    result = [spelling_mistake_func(m, r) for m, r in zip(m_data, r_data)]
    answer = [False, True, False]
    assert np.all(answer == answer)


def test_1_spelling_mistake(row):
    r_data = row.copy()
    r_data[10] = str(row[10])[::-1]
    raw_score, count, c_data = correct_words(row, r_data, N, None, spelling_mistake_func)
    assert raw_score == N - 1
    assert count['spelling_mistake'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'spelling_mistake'}


def test_2_spelling_mistake(row):
    r_data = row.copy()
    r_data[10] = str(row[10])[::-1]
    r_data[12] = str(row[12])[::-1]
    raw_score, count, c_data = correct_words(row, r_data, N, None, spelling_mistake_func)
    assert raw_score == N - 2
    assert count['spelling_mistake'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'spelling_mistake'}


def test_1_gap_3_spelling_mistake(row):
    r_data = row.copy()
    r_data[10] = str(row[10])[::-1]
    r_data[11] = ''
    r_data[12] = str(row[12])[::-1]
    r_data[13] = str(row[13])[::-1]
    raw_score, count, c_data = correct_words(row, r_data, 20, None, spelling_mistake_func)
    assert raw_score == 20 + 10 - 3
    assert count['spelling_mistake'] == 3
    assert count['correct'] == N - 4
    assert count['gap'] == 1
    assert {e.name for e in c_data} == {'gap', 'correct', 'spelling_mistake'}


def test_1_wrong_3_spelling_mistake(row):
    r_data = row.copy()
    r_data[10] = str(row[10])[::-1]
    r_data[11] = 'wrong'
    r_data[12] = str(row[12])[::-1]
    r_data[13] = str(row[13])[::-1]
    raw_score, count, c_data = correct_words(row, r_data, 20, None, spelling_mistake_func)
    assert raw_score == 20 + 10 - 3
    assert count['spelling_mistake'] == 3
    assert count['correct'] == N - 4
    assert count['wrong'] == 1
    assert {e.name for e in c_data} == {'wrong', 'correct', 'spelling_mistake'}


def test_2_wrong_2_spelling_mistake(row):
    r_data = row.copy()
    r_data[10] = str(row[10])[::-1]
    r_data[11] = 'wrong'
    r_data[12] = 'wrong'
    r_data[13] = str(row[13])[::-1]
    raw_score, count, c_data = correct_words(row, r_data, 20, None, spelling_mistake_func)
    assert raw_score == 20
    assert count['spelling_mistake'] == 2
    assert count['correct'] == N - 4
    assert count['wrong'] == 2
    assert {e.name for e in c_data} == {'wrong', 'correct', 'spelling_mistake'}


def test_spelling_mistake_mix(row):
    r_data = row.copy()

    # First row
    r_data[1] = 'wrong'

    # Second row
    r_data[20] = str(row[20])[::-1]
    r_data[21] = 'wrong'
    r_data[-3:] = ['']*3
    raw_score, count, c_data = correct_words(row, r_data, 20, None, spelling_mistake_func)
    assert raw_score == 10 + 9 - 1
    assert count['spelling_mistake'] == 1
    assert count['correct'] == N - 6
    assert count['wrong'] == 2
    assert count['not_reached'] == 3
    assert {e.name for e in c_data} == {'wrong', 'correct', 'spelling_mistake',
                                        'not_reached'}


def test_spelling_mistake_mix2(row):
    r_data = row.copy()

    # First row
    r_data[10] = str(row[10])[::-1]
    r_data[19] = ''

    # Second row
    r_data[20] = str(row[20])[::-1]
    r_data[21] = str(row[21])[::-1]
    r_data[-6:] = ['']*6
    raw_score, count, c_data = correct_words(row, r_data, 20, None, spelling_mistake_func)
    assert raw_score == 9 + 12
    assert count['spelling_mistake'] == 3
    assert count['correct'] == N - 10
    assert count['not_reached'] == 6
    assert count['gap'] == 1
    assert {e.name for e in c_data} == {'correct', 'spelling_mistake', 'gap',
                                        'not_reached'}


def test_spelling_mistake_mix3(row):
    r_data = row.copy()

    # First row
    r_data[0] = 'wrong'
    r_data[1] = ''

    # Second row
    r_data[10] = str(r_data[10])[::-1]
    r_data[12] = str(r_data[12])[::-1]
    r_data[13] = str(r_data[13])[::-1]

    # Third row
    for i in range(20, 30):
        r_data[i] = str(row[i])[::-1]
    r_data[22] = 'wrong'

    # Forth row
    r_data[30] = 'wrong'
    r_data[31] = str(row[31])[::-1]
    r_data[33:] = ['']*7

    raw_score, count, c_data = correct_words(row, r_data, 10, None, spelling_mistake_func)
    assert raw_score == 0 + 7 + 0 + 1
    assert count['spelling_mistake'] == 3 + 9 + 1
    assert count['correct'] == 8 + 7 + 0 + 1
    assert count['not_reached'] == 7
    assert count['gap'] == 1
    assert count['wrong'] == 3
    assert {e.name for e in c_data} == {'correct', 'spelling_mistake', 'gap',
                                        'not_reached', 'wrong'}


def test_naive_spellcheck():
    result = [naive_spellcheck(m, r) for m, r in zip(
        ['daniel', 'andersson', 'olofsbäck',  'hej', 'tiger'],
        ['daniel', 'anderson',  'olofsbäckk', 'häj', 'nasse']
    )]
    assert list(result) == [True, True, True, False, False]


def test_naive_spellcheck_one_char_wrong():
    assert naive_spellcheck('daniel', 'Daniel') == True
    assert naive_spellcheck('daniel', 'danieI') == True
    assert naive_spellcheck('daniel', 'danieI') == True


def test_naive_spellcheck_one_char_extra():
    assert naive_spellcheck('daniel', 'daniell') == True
    assert naive_spellcheck('daniel', 'hdaniel') == True
    assert naive_spellcheck('daniel', 'danjiel') == True


def test_naive_spellcheck_one_char_missing():
    assert naive_spellcheck('daniel', 'danie') == True
    assert naive_spellcheck('daniel', 'aniel') == True
    assert naive_spellcheck('daniel', 'danel') == True


def test_naive_spellcheck_short_words():
    assert naive_spellcheck('a', 'x') == False
    assert naive_spellcheck('ab', 'ax') == False
    assert naive_spellcheck('abc', 'abx') == False
    assert naive_spellcheck('abcd', 'abcx') == False
    assert naive_spellcheck('abcde', 'abcex') == False
    assert naive_spellcheck('abcdef', 'abcdefx') == True


def test_naive_spellcheck_really_wrong():
    assert naive_spellcheck('abc', 'abc') == True
    assert naive_spellcheck('andersson', 'andersson') == True

    assert naive_spellcheck('abcddfsef', 'abfwefcdefx') == False
    assert naive_spellcheck('abcdef', 'abcdefff') == False
    assert naive_spellcheck('abcdef', 'aaabcdef') == False
    assert naive_spellcheck('abcdéf', 'ábcdef') == False


def test_exactly_1_wrong():
    raw_score, count, c_data = correct_words(['a']*40, ['b'] + ['']*39, 40)
    assert raw_score == 0
    assert count['wrong'] == 1
    assert count['not_reached'] == 39
    assert {e.name for e in c_data} == {'wrong', 'not_reached'}


def test_equivalent_words(row):
    r_data = row.copy()
    r_data[0] = 'äpple'
    f = lambda a, b: a == '0' and b == 'äpple'
    raw_score, count, c_data = correct_words(row, r_data, 20, equivalent_func=f)
    assert raw_score == 40
    assert count['correct'] == N
    assert count['wrong'] == 0
    assert {e.name for e in c_data} == {'correct'}


def test_equiv_and_ac_word_same(row):
    # Test that a word which is marked as "correct" and has higher
    # precedence than "spelling_mistake"
    r_data = row.copy()
    r_data[0] = 'äpple'
    f = lambda a, b: a == '0' and b == 'äpple'
    raw_score, count, c_data = correct_words(row, r_data, 20, f, f)
    assert raw_score == 40
    assert count['correct'] == N
    assert count['wrong'] == 0
    assert {e.name for e in c_data} == {'correct'}


def test_1_memo_not_available(row):
    r_data = row.copy()
    m_data = row
    m_data[0] = ''
    raw_score, count, c_data = correct_words(m_data, r_data, N)
    assert raw_score == N/2
    assert count['gap'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_1_memo_not_available_last(row):
    r_data = row.copy()
    m_data = row
    m_data[-1] = ''
    raw_score, count, c_data = correct_words(m_data, r_data, N)
    assert raw_score == N - 1
    assert count['not_reached'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'not_reached'}