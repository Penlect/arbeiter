import pytest
from arbeiter import correct_spoken

N = 40


def _raw_score(c_data):
    c_data = c_data.copy()
    output = 0
    for e in c_data:
        if e.name == 'correct':
            output += 1
        else:
            break
    return output


def test_all_correct(row):
    raw_score, count, c_data = correct_spoken(row, row)
    assert raw_score == N
    assert count['correct'] == N
    assert {e.name for e in c_data} == {'correct'}


def test_1_gap(row, row_1_gap):
    raw_score, count, c_data = correct_spoken(row, row_1_gap)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['gap'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_2_gap(row, row_2_gap):
    raw_score, count, c_data = correct_spoken(row, row_2_gap)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['gap'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'gap'}


def test_1_wrong(row, row_1_wrong):
    raw_score, count, c_data = correct_spoken(row, row_1_wrong)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['wrong'] == 1
    assert count['correct'] == N - 1
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_2_wrong(row, row_2_wrong):
    raw_score, count, c_data = correct_spoken(row, row_2_wrong)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['wrong'] == 2
    assert count['correct'] == N - 2
    assert {e.name for e in c_data} == {'correct', 'wrong'}


def test_all_not_reached(row, row_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_not_reached)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['not_reached'] == N
    assert {e.name for e in c_data} == {'not_reached'}


def test_7_not_reached(row, row_7_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_7_not_reached)
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_not_reached(row, row_1_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_1_not_reached)
    assert raw_score == N - 1
    assert count['correct'] == raw_score
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_1_gap_1_not_reached(row, row_1_gap_1_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_1_gap_1_not_reached)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['correct'] == N - 2
    assert count['gap'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_1_wrong_1_not_reached(row, row_1_wrong_1_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_1_wrong_1_not_reached)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['correct'] == N - 2
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_2_gap_1_not_reached(row, row_2_gap_1_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_2_gap_1_not_reached)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['correct'] == N - 3
    assert count['gap'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'gap', 'not_reached'}


def test_2_wrong_1_not_reached(row, row_2_wrong_1_not_reached):
    raw_score, count, c_data = correct_spoken(row, row_2_wrong_1_not_reached)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['correct'] == N - 3
    assert count['wrong'] == 2
    assert count['not_reached'] == 1
    assert {e.name for e in c_data} == {'correct', 'wrong', 'not_reached'}


def test_mix(row, row_1_gap, row_2_gap, row_1_wrong, row_2_wrong,
             row_not_reached, row_7_not_reached):
    memo_data = row*7
    recall_data = (row + row_1_gap + row_2_gap + row_1_wrong + row_2_wrong +
                   row_7_not_reached + row_not_reached)
    raw_score, count, c_data = correct_spoken(memo_data, recall_data)
    c_data = list(c_data)
    assert raw_score == _raw_score(c_data)
    assert count['gap'] == 3
    assert count['wrong'] == 3
    assert count['not_reached'] == 7 + N
    assert count['correct'] == N*6 - 1 - 2 - 1 - 2 - 7
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}


def test_shorter(row):
    raw_score, count, c_data = correct_spoken(row, row[:-7])
    assert raw_score == N - 7
    assert count['correct'] == raw_score
    assert count['not_reached'] == 7
    assert {e.name for e in c_data} == {'correct', 'not_reached'}


def test_too_long_recall(row):
    with pytest.raises(ValueError):
        correct_spoken(row, row*2)


def test_empty():
    with pytest.raises(ValueError):
        correct_spoken([], [])
