
import numpy as np
import arbeiter


def test_get_binary():
    output = arbeiter.get_binary(123)
    assert len(output) == 123
    assert set(output) == {0, 1}


def test_get_numbers():
    output = arbeiter.get_numbers(1234)
    assert len(output) == 1234
    assert set(output) <= set(list(range(10)))


def test_get_cards():
    output = arbeiter.get_cards(3)

    assert len(output[52*0:52*1]) == 52
    assert set(output[52*0:52*1]) == set(list(range(52)))

    assert len(output[52*1:52*2]) == 52
    assert set(output[52*1:52*2]) == set(list(range(52)))

    assert len(output[52*2:52*3]) == 52
    assert set(output[52*2:52*3]) == set(list(range(52)))


def test_get_words():
    output = arbeiter.get_words(['daniel', 'andersson'])
    assert ['daniel', 'andersson'] == list(output)


def test_get_dates():
    stories = ['Olofsbäcks gård']*1100
    output = arbeiter.get_dates(stories)
    assert len(output) == len(stories)
    assert set(output['shuffle_index']) == set(list(range(len(stories))))
    assert output['date'].min() >= 1000
    assert output['date'].max() < 2100
    assert set(output['story']) == {'Olofsbäcks gård'}


def test_get_images():
    files = [f'file_{i:02}' for i in range(10)]
    output = arbeiter.get_images(files)
    assert len(output) == len(files)
    assert files == [i[1] for i in output]
    assert {1, 2, 3, 4, 5} == set([i[0] for i in output][0:5])
    assert {1, 2, 3, 4, 5} == set([i[0] for i in output][5:10])


def test_get_names():
    files = [f'file_{i:02}' for i in range(10)]
    firstnames = ['Daniel']*10
    lastnames = ['Andersson']*10
    output = arbeiter.get_names(files, firstnames, lastnames)
    assert len(output) == len(files)
    assert files == [i[1] for i in output]
    assert firstnames == [i[2] for i in output]
    assert lastnames == [i[3] for i in output]
