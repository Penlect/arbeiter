
import pytest
from arbeiter import correct_names, remove_diaeresis

m_data = ['Zidane', 'Vivi', 'Garnet', 'Steiner', 'Freya', 'Quina', 'Eiko',
          'Amarant', 'Kuja', 'Garland']


def test_all_correct():
    r_data = m_data
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct'}
    assert count['correct'] == 10
    assert raw_score == 10


def test_1_gap():
    r_data = m_data[:]
    r_data[0] = ''
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap'}
    assert count['correct'] == 9
    assert count['gap'] == 1
    assert raw_score == 9


def test_2_gap():
    r_data = m_data[:]
    r_data[0] = ''
    r_data[3] = ''
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap'}
    assert count['correct'] == 8
    assert count['gap'] == 2
    assert raw_score == 8


def test_1_wrong():
    r_data = m_data[:]
    r_data[2] = 'Aerith'
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 9
    assert count['wrong'] == 1
    assert raw_score == 9


def test_2_wrong():
    r_data = m_data[:]
    r_data[0] = 'Cloud'
    r_data[2] = 'Aerith'
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 8
    assert count['wrong'] == 2
    assert raw_score == 8


def test_all_not_reached():
    r_data = ['']*10
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'not_reached'}
    assert count['not_reached'] == 10
    assert raw_score == 0


def test_3_not_reached():
    r_data = m_data[:]
    r_data[-1] = ''
    r_data[-2] = ''
    r_data[-3] = ''
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'not_reached'}
    assert count['correct'] == 7
    assert count['not_reached'] == 3
    assert raw_score == 7


def test_1_gap_1_wrong_1_not_reached():
    r_data = m_data[:]
    r_data[0] = ''
    r_data[1] = 'Barret'
    r_data[-1] = ''
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap', 'wrong',
                                        'not_reached'}
    assert count['gap'] == 1
    assert count['wrong'] == 1
    assert count['not_reached'] == 1
    assert count['correct'] == 7
    assert raw_score == 7


def test_same_name_2_times():
    r_data = m_data[:]
    r_data[1] = r_data[0]
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 9
    assert count['wrong'] == 1
    assert raw_score == 9


def test_same_name_3_times():
    r_data = m_data[:]
    r_data[1] = r_data[0]
    r_data[2] = r_data[0]
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 8
    assert count['wrong'] == 2
    assert raw_score == 8


def test_same_name_4_times():
    r_data = m_data[:]
    r_data[1] = r_data[0]
    r_data[2] = r_data[0]
    r_data[3] = r_data[0]
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 7
    assert count['wrong'] == 3
    assert raw_score == 6


def test_same_name_4_wrong_times():
    r_data = m_data[:]
    r_data[0] = 'Sephiroth'
    r_data[1] = 'Sephiroth'
    r_data[2] = 'Sephiroth'
    r_data[3] = 'Sephiroth'
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 6
    assert count['wrong'] == 4
    assert raw_score == 5


def test_same_name_5_wrong_times():
    r_data = m_data[:]
    r_data[0] = 'Sephiroth'
    r_data[1] = 'Sephiroth'
    r_data[2] = 'Sephiroth'
    r_data[3] = 'Sephiroth'
    r_data[4] = 'Sephiroth'
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 5
    assert count['wrong'] == 5
    assert raw_score == 4


def test_same_name_6_wrong_times():
    r_data = m_data[:]
    r_data[0] = 'Sephiroth'
    r_data[1] = 'Sephiroth'
    r_data[2] = 'Sephiroth'
    r_data[3] = 'Sephiroth'
    r_data[4] = 'Sephiroth'
    r_data[5] = 'Sephiroth'
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'wrong'}
    assert count['correct'] == 4
    assert count['wrong'] == 6
    assert raw_score == 2


def test_same_name_all_wrong():
    r_data = ['Sephiroth']*len(m_data)
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'wrong'}
    assert count['wrong'] == 10
    assert raw_score == 0


def test_too_long_recall():
    with pytest.raises(ValueError):
        correct_names(m_data, m_data*2)


def test_empty():
    with pytest.raises(ValueError):
        correct_names([], [])


def test_remove_diaeresis():
    # Swedish:
    assert remove_diaeresis('åäö', 'aao') == True
    # French:
    assert remove_diaeresis('é', 'e') == True
    assert remove_diaeresis('à, è, ù', 'a, e, u') == True
    assert remove_diaeresis('â, ê, î, ô, û', 'a, e, i, o, u') == True
    assert remove_diaeresis('ç', 'c') == True
    assert remove_diaeresis('ë, ï, ü', 'e, i, u') == True


def test_equivalent_name():
    r_data = m_data.copy()
    r_data[0] = 'Zidané'
    r_data[2] = 'Gårnet'
    r_data[8] = 'Küja'
    raw_score, count, c_data = correct_names(m_data, r_data, remove_diaeresis)
    assert {e.name for e in c_data} == {'correct'}
    assert count['correct'] == 10
    assert raw_score == 10


def test_not_available():
    m_data = list('abcdefghi') + ['']*4 + ['n']
    r_data = list('abcdefghijklmn')
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'gap'}
    assert count['gap'] == 4
    assert raw_score == 10


def test_not_available_last():
    m_data = list('abcdefghijk') + ['']*3
    r_data = list('abcdefghijklmn')
    raw_score, count, c_data = correct_names(m_data, r_data)
    assert {e.name for e in c_data} == {'correct', 'not_reached'}
    assert count['not_reached'] == 3
    assert raw_score == 11