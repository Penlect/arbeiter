
The arbeiter package
--------------------

This package is used to correct and compute raw score of memory competitor's
recall data.

These disciplines are supported

* Numbers
* Binary Numbers
* Words
* Historical Dates
* Cards
* Images
* Names & Faces
