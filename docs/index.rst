.. Arbiter documentation master file, created by
   sphinx-quickstart on Fri Oct 26 17:39:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Arbiter's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   arbiter


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
